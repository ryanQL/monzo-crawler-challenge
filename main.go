package main

import (
	"fmt"
	"sitemap/argv"
	"sitemap/crawler"
	"sitemap/options"
	"sitemap/validators"
)

func main() {
	opts := argv.Parse()
	u, d, f := validators.ValidateInputParams(&opts)
	options := options.CrawlOpts{
		URL:      u,
		Depth:    d,
		Filename: f,
		Storage:  options.Enum.File,
	}

	fmt.Printf("Crawling %s \n", u.Hostname())
	crawler.Run(options)
	fmt.Println("Done")
	crawler.Sitemap(options)
	crawler.Stats()
	fmt.Printf("Sitemap generated: %s \n", options.Filename)
}
