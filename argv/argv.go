package argv

import docopt "github.com/docopt/docopt-go"

// Parse define the interface for your command-line app, and automatically generate a parser.
func Parse() docopt.Opts {
	usage := `Simple Web Crawler. Generates a text sitemap.

	Usage:
		sitemap <url>
		sitemap <url> [--depth=<number>] [--output=<filepath>]
		sitemap -h | --help
	
	Options:
		-h --help           Show this screen.
		--depth=<n>         Crawl depth [default: 1] 
		--output=<filepath> Sitemap file path [default: hostname.txt]
		
	`
	opts, _ := docopt.ParseDoc(usage)
	return opts
}
