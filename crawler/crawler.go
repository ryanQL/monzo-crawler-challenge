package crawler

import (
	"fmt"
	"net/http"
	"sitemap/options"
	"sitemap/stats"
	"sitemap/storage/file"
	"strings"
	"sync"
	"time"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/debug"
)

var (
	m       sync.Map
	elapsed time.Duration
)

//Run Crawl a website
func Run(o options.CrawlOpts) {
	start := time.Now()

	c := colly.NewCollector(
		colly.AllowedDomains(o.URL.Host),
		colly.MaxDepth(o.Depth),
		colly.Async(true),
		colly.Debugger(&debug.LogDebugger{}),
	)

	c.Limit(&colly.LimitRule{
		DomainGlob:  fmt.Sprintf("*%s/*", o.URL.Host),
		Parallelism: 10,
		Delay:       5 * time.Second,
		RandomDelay: 5 * time.Second,
	})

	c.AllowURLRevisit = false

	c.WithTransport(&http.Transport{
		DisableKeepAlives: true,
	})

	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		absoluteURL := e.Request.AbsoluteURL(link)

		if strings.HasPrefix(absoluteURL, o.URL.String()) {
			m.Store(absoluteURL, absoluteURL)
			e.Request.Visit(link)
		}
	})

	// Start scraping
	c.Visit(o.URL.String())

	// Wait until threads are finished
	c.Wait()

	elapsed = time.Since(start)
}

//Sitemap prints out the links
func Sitemap(o options.CrawlOpts) {
	file.Sitemap(&m, o.Filename)
}

// Stats shows crawler stats
func Stats() {
	fmt.Printf("Execution time: %f second(s)\n", elapsed.Seconds())

	i := 0

	m.Range(func(k, v interface{}) bool {
		i++
		return true
	})

	fmt.Printf("Found %d link(s) \n", i)
	stats.PrintMemUsage()
}
