package validators

import (
	"fmt"
	"net/url"
	"regexp"

	docopt "github.com/docopt/docopt-go"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// ValidateInputParams validate input parameters
func ValidateInputParams(opts *docopt.Opts) (*url.URL, int, string) {
	urlParam, _ := opts.String("<url>")
	err := validation.Validate(urlParam,
		validation.Required, // not empty
		is.URL,              // is a valid URL
		validation.Match(regexp.MustCompile("^(http|https)://")).Error("must start with a scheme"),
	)

	if err != nil {
		panic(err)
	}

	u, err := url.Parse(urlParam)
	if err != nil {
		panic(err)
	}

	d, err := opts.Int("--depth")
	if err != nil {
		d = 2
	}

	if d < 2 {
		d = 2
	}

	f, err := opts.String("--filepath")
	if err != nil {
		f = fmt.Sprintf("./sitemaps/%s.txt", u.Host)
	}

	return u, d, f
}
