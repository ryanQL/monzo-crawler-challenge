package options

import "net/url"

// CustomStorage sets the custom storage strategy for the crawler
type CustomStorage = int

type list struct {
	File     CustomStorage
	Database CustomStorage
	Memory   CustomStorage
}

// Enum for public use
var Enum = &list{
	File:     0,
	Database: 1,
	Memory:   2,
}

// CrawlOpts stores the URL and the crawl depth values
type CrawlOpts struct {
	URL      *url.URL
	Depth    int
	Filename string
	Storage  CustomStorage
}

// Link stores link info
type Link struct {
	Target string
}
